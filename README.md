# Cobalt: Documentation

Cobalt is a collection of open data web APIs that allow you to interface with public information from the University of Toronto.

Cobalt is designed to foster a culture of open data at the university. Cobalt's web APIs are free to use and also free to contribute to.

#### Getting started
* [Introduction](./getting-started/introduction.md)
* [Authenticating](./getting-started/authenticating.md)
* [Self hosting](./getting-started/self-hosting.md)
* [Contributing](./getting-started/contributing.md)

#### Course API

* [Introduction](./endpoints/courses/introduction.md)
* [GET courses/:id](./endpoints/courses/show.md)
* [GET courses/list](./endpoints/courses/list.md)
* [GET courses/search](./endpoints/courses/search.md)
* [GET courses/filter](./endpoints/courses/filter.md)

#### Building API

* [Introduction](./endpoints/buildings/introduction.md)
* [GET buildings/:id](./endpoints/buildings/show.md)
* [GET buildings/list](./endpoints/buildings/list.md)
* [GET buildings/search](./endpoints/buildings/search.md)
* [GET buildings/filter](./endpoints/buildings/filter.md)
